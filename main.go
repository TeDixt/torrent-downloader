package main

import (
	"io/ioutil"

	torrent "gitlab.com/tedixt/torrent-downloader/torrent"
)

const testTorrent = "test.torrent"

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data, err := ioutil.ReadFile(testTorrent)
	check(err)
	t := torrent.ReadFromBytes(&data)
	t.Start()
}
