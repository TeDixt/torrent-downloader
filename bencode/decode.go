package bencoder

import (
	"strconv"
)

const (
	singleMode   int64  = 1
	multipleMode int64  = 2
	getValue     string = "getValue"
	getType      string = "getType"
	getLength    string = "getLength"
	getEnd       string = "getEnd"
)

// BType is bencoded elem interface
type BType interface {
	parse()
	Raw() []byte
}

// BBytes is bencoded bytes (string)
type BBytes struct {
	value    []byte
	Parsed   string
	length   int64
	isParsed bool
}

func (b *BBytes) parse() {
	b.Parsed = string(b.value)
	b.isParsed = true
}

// Raw returns raw bytes value of BBytes
func (b *BBytes) Raw() []byte {
	return b.value
}

// BInt is bencoded integer
type BInt struct {
	value    []byte
	Parsed   int64
	isParsed bool
}

func (b *BInt) parse() {
	value := string(b.value)
	Parsed, _ := strconv.ParseInt(value, 10, 64)
	b.Parsed = Parsed
	b.isParsed = true
}

// Raw returns raw bytes value of BInt
func (b *BInt) Raw() []byte {
	return b.value
}

// BList is bencoded list of BTypes
type BList struct {
	value    []BType
	Parsed   []BType
	RawList  []byte
	isParsed bool
}

func (b *BList) parse() {
	b.Parsed = b.value
	b.isParsed = true
}

// Raw returns raw bytes value of BList
func (b *BList) Raw() []byte {
	return b.RawList
}

// BDict is bencoded mapping
type BDict struct {
	value    []BType
	RawDict  []byte
	Parsed   map[string]BType
	isParsed bool
}

func (b *BDict) parse() {
	Parsed := make(map[string]BType)
	var key *BBytes
	data := b.value
	for idx, elem := range data {
		elem.parse()
		if idx%2 == 0 {
			key, _ = elem.(*BBytes)
		} else {
			Parsed[key.Parsed] = elem
		}
	}
	b.Parsed = Parsed
	b.isParsed = true
}

// Raw returns raw bytes value of BDict
func (b *BDict) Raw() []byte {
	return b.RawDict
}

// ParseData Parses bencoded data to slice of BTypes
func ParseData(data *[]byte, cursor int) ([]BType, int) {
	var elemList []BType
	var currentType string
	var currentLength string
	var parsedCount int64
	var currentBytes *BBytes
	var currentInt *BInt
	var currentList *BList
	var currentDict *BDict
	var parsedValue []BType
	state := getType
	var idx int
	//parseRawDict := false
	rawIndex := 0

	for idx = cursor; idx < len(*data); idx++ {
		valueRaw := (*data)[idx]
		value := string(valueRaw)
		if state == getType {
			_, err := strconv.ParseInt(value, 10, 64)
			if err != nil {
				currentType = value
				state = getValue
				if currentType == "i" {
					currentInt = &BInt{}
				} else if currentType == "l" {
					currentList = &BList{}
					rawIndex = idx
				} else if currentType == "d" {
					currentDict = &BDict{}
					rawIndex = idx
				} else if currentType == "e" {
					break
				}
			} else {
				currentBytes = &BBytes{}
				currentLength = value
				currentType = "b"
				state = getLength
				parsedCount = 0
			}

		} else if state == getLength {
			if value == ":" {
				length, _ := strconv.ParseInt(currentLength, 10, 64)
				currentBytes.length = length
				state = getValue
				currentLength = ""
			} else {
				currentLength += value
			}
		} else if state == getValue {
			if currentType == "b" {
				currentBytes.value = append(currentBytes.value, valueRaw)
				parsedCount++
				if parsedCount >= currentBytes.length {
					currentBytes.parse()
					//if currentBytes.Parsed == "info" {
					//parseRawDict = true
					//}
					elemList = append(elemList, currentBytes)
					state = getType
				}
			}
			if currentType == "i" {
				if value == "e" {
					currentInt.parse()
					elemList = append(elemList, currentInt)
					state = getType
				} else {
					currentInt.value = append(currentInt.value, valueRaw)
				}
			}
			if currentType == "l" {
				parsedValue, idx = ParseData(data, idx)
				currentList.RawList = (*data)[rawIndex : idx+1]
				currentList.value = parsedValue
				currentList.parse()
				elemList = append(elemList, currentList)
				state = getType

			}
			if currentType == "d" {
				parsedValue, idx = ParseData(data, idx)
				//if parseRawDict {
				currentDict.RawDict = (*data)[rawIndex : idx+1]
				//parseRawDict = false
				//}
				currentDict.value = parsedValue
				currentDict.parse()
				elemList = append(elemList, currentDict)
				state = getType
			}
		}
	}

	return elemList, idx
}
