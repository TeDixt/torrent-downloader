package torrent

import (
	"crypto/sha1"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"

	bencoder "gitlab.com/tedixt/torrent-downloader/bencode"
)

const (
	singleMode     int64  = 1
	multipleMode   int64  = 2
	clientIDPrefix string = "-GT0001"
	clientPort     string = "6881"
)

// File is torrent file format
type File struct {
	length int64
	md5sum string
	path   []string
}

// Info is torrent info format
type Info struct {
	pieceLength int64
	pieces      string
	private     int64
	mode        int64
	name        string
	length      int64
	md5sum      string
	files       []File
	raw         []byte
}

// Hash is method for getting info hash
func (i *Info) Hash() []byte {
	h := sha1.New()
	h.Write(i.raw)
	bs := h.Sum(nil)
	return bs
}

// Torrent is torrent bencoded format
type Torrent struct {
	announce     string
	announceList [][]string
	info         Info
	creationDate int64
	comment      string
	createdBy    string
	encoding     string
}

// Load is method for loading torrent from bencoded data
func (t *Torrent) Load(data []bencoder.BType) {
	root, _ := data[0].(*bencoder.BDict)
	rootMap := root.Parsed
	announce, _ := rootMap["announce"].(*bencoder.BBytes)
	t.announce = announce.Parsed
	announceBList, ok := rootMap["announce-list"].(*bencoder.BList)
	if ok {
		var announceList [][]string
		announceParsed := announceBList.Parsed
		for _, announceElem := range announceParsed {
			var announcePart []string
			announceInnerBList, _ := announceElem.(*bencoder.BList)
			announceInner := announceInnerBList.Parsed
			for _, announceInnerPart := range announceInner {
				announceInnerPartBytes := announceInnerPart.(*bencoder.BBytes)
				announceInnerParsed := announceInnerPartBytes.Parsed
				announcePart = append(announcePart, announceInnerParsed)
			}
			announceList = append(announceList, announcePart)
		}
		t.announceList = announceList
	}
	creationDate, ok := rootMap["creation date"].(*bencoder.BInt)
	if ok {
		t.creationDate = creationDate.Parsed
	}
	createdBy, ok := rootMap["created by"].(*bencoder.BBytes)
	if ok {
		t.createdBy = createdBy.Parsed
	}
	encoding, ok := rootMap["encoding"].(*bencoder.BBytes)
	if ok {
		t.encoding = encoding.Parsed
	}
	comment, ok := rootMap["comment"].(*bencoder.BBytes)
	if ok {
		t.comment = comment.Parsed
	}
	info := Info{}
	infoRaw := rootMap["info"].(*bencoder.BDict)
	info.raw = infoRaw.Raw()
	infoMap := infoRaw.Parsed
	pieceLength, _ := infoMap["piece length"].(*bencoder.BInt)
	info.pieceLength = pieceLength.Parsed
	pieces, _ := infoMap["pieces"].(*bencoder.BBytes)
	info.pieces = pieces.Parsed
	private, ok := infoMap["private"].(*bencoder.BInt)
	if ok {
		info.private = private.Parsed
	}
	length, ok := infoMap["length"].(*bencoder.BInt)
	if ok {
		info.mode = singleMode
		info.length = length.Parsed
		md5sum, ok := infoMap["md5sum"].(*bencoder.BBytes)
		if ok {
			info.md5sum = md5sum.Parsed
		}
	} else {
		info.mode = multipleMode
		var tFiles []File
		files, _ := infoMap["files"].(*bencoder.BList)
		filesList := files.Parsed
		for _, file := range filesList {
			fileBDict, _ := file.(*bencoder.BDict)
			fileMap := fileBDict.Parsed
			tFile := File{}
			length, _ := fileMap["length"].(*bencoder.BInt)
			tFile.length = length.Parsed
			md5sum, ok := fileMap["md5sum"].(*bencoder.BBytes)
			if ok {
				tFile.md5sum = md5sum.Parsed
			}
			path, _ := fileMap["path"].(*bencoder.BList)
			pathParsed := path.Parsed
			var pathList []string
			for _, pathElem := range pathParsed {
				pathBytes, _ := pathElem.(*bencoder.BBytes)
				pathString := pathBytes.Parsed
				pathList = append(pathList, pathString)
			}
			tFile.path = pathList
			tFiles = append(tFiles, tFile)
		}
		info.files = tFiles
	}
	name, _ := infoMap["name"].(*bencoder.BBytes)
	info.name = name.Parsed
	t.info = info
}

// Start prepares request for getting torrent info
func (t *Torrent) Start() TrackerResponse {
	queryString := url.Values{}
	queryString.Set("peer_id", string(generateClientID()))
	queryString.Set("info_hash", string(t.info.Hash()))
	queryString.Set("port", clientPort)
	queryString.Set("uploaded", "0")
	queryString.Set("downloaded", "0")
	queryString.Set("event", "started")
	queryString.Set("left", fmt.Sprintf("%v", t.info.pieceLength))
	startURL := fmt.Sprintf("%s?%s", t.announce, queryString.Encode())
	response, _ := http.Get(startURL)
	responseBody, _ := ioutil.ReadAll(response.Body)
	responseData, _ := bencoder.ParseData(&responseBody, 0)
	tr := TrackerResponse{}
	tr.Load(responseData)
	return tr
}

// Peer is torrent tracker peer
type Peer struct {
	PeerID string
	IP     string
	Port   uint16
}

// TrackerResponse is response from torrent tracker
type TrackerResponse struct {
	FailureReason  string
	WarningMessage string
	Interval       int64
	MinInterval    int64
	TrackerID      string
	Complete       int64
	Incomplete     int64
	Peers          []Peer
}

// Load is method for loading torrent from bencoded data
func (tr *TrackerResponse) Load(data []bencoder.BType) {
	root, _ := data[0].(*bencoder.BDict)
	rootMap := root.Parsed
	failureReason, ok := rootMap["failure reason"].(*bencoder.BBytes)
	if ok {
		tr.FailureReason = failureReason.Parsed
		return
	}
	warningMessage, ok := rootMap["failure reason"].(*bencoder.BBytes)
	if ok {
		tr.WarningMessage = warningMessage.Parsed
	}
	interval, _ := rootMap["interval"].(*bencoder.BInt)
	tr.Interval = interval.Parsed
	minInterval, ok := rootMap["min interval"].(*bencoder.BInt)
	if ok {
		tr.MinInterval = minInterval.Parsed
	}
	tracker, ok := rootMap["tracker id"].(*bencoder.BBytes)
	if ok {
		tr.TrackerID = tracker.Parsed
	}
	complete, _ := rootMap["complete"].(*bencoder.BInt)
	tr.Complete = complete.Parsed
	incomplete, _ := rootMap["incomplete"].(*bencoder.BInt)
	tr.Incomplete = incomplete.Parsed
	var peers []Peer
	peersRaw, ok := rootMap["peers"].(*bencoder.BList)
	if ok {
		peersParsed := peersRaw.Parsed
		for _, peerData := range peersParsed {
			peerDict, _ := peerData.(*bencoder.BDict)
			peerDictParsed := peerDict.Parsed
			peer := Peer{}
			peerID := peerDictParsed["peer id"].(*bencoder.BBytes)
			peer.PeerID = peerID.Parsed

			ip := peerDictParsed["ip"].(*bencoder.BBytes)
			peer.IP = ip.Parsed

			port := peerDictParsed["port"].(*bencoder.BInt)
			peer.Port = uint16(port.Parsed)
			peers = append(peers, peer)
		}
	} else {
		peersBBytes := rootMap["peers"].(*bencoder.BBytes)
		peersArray := peersBBytes.Raw()
		for i := 0; i < len(peersArray); i += 6 {
			peer := Peer{}
			ipPortSlice := peersArray[i : i+6]
			ipSlice := ipPortSlice[0:4]
			portSlice := ipPortSlice[4:6]
			ip := net.IP(ipSlice)
			ipString := ip.String()
			port := binary.BigEndian.Uint16(portSlice)
			peer.IP = ipString
			peer.Port = port
			peers = append(peers, peer)
		}

	}
	tr.Peers = peers
}

func generateClientID() []byte {
	clientID := clientIDPrefix + "32RGWAGA4TAW"
	result := make([]byte, 20, 20)
	for i, x := range clientID {
		result[i] = byte(x)
	}
	return result
}

// ReadFromBytes decodes torrent file from bytes
func ReadFromBytes(torrentData *[]byte) *Torrent {
	torrent := &Torrent{}
	data, _ := bencoder.ParseData(torrentData, 0)
	torrent.Load(data)
	return torrent
}
